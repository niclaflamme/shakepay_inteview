import React from 'react';
import { XYPlot, XAxis, YAxis, HorizontalGridLines, LineSeries } from 'react-vis';


const Graph = ({ data, xAxisTile, yAxisTile }) => (
  <XYPlot
    width={700}
    height={700}
  >
    <LineSeries
      color="red"
      data={data}
     />

    <XAxis title={xAxisTile} />
    <YAxis title={yAxisTile} />
    <HorizontalGridLines />
  </XYPlot>
);

export default Graph;
