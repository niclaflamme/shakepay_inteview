import React, { useState, useEffect } from 'react';
import axios from 'axios';
import moment from 'moment';
import sortBy from 'lodash.sortby';

import Graph from '../../components/Graph';
import { balancesToNetWorth, transactionsToBalancesHistory } from '../../utils';


const rates = {
  BTC_CAD_rate: 13009.42,
  ETH_CAD_rate: 325.51,
};


const App = () => {
  const [transactions, setTransactions] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const { data } = await axios.get('https://shakepay.github.io/programming-exercise/web/transaction_history.json');
      const sortedTransactions = sortBy(data, ['createdAt']);
      setTransactions(sortedTransactions);
    };

    fetchData();
  }, []);

  const balancesHistory = transactionsToBalancesHistory({ transactions });

  const netWorthData = balancesHistory
    .map((balances) => {
      const timeStamp = moment(balances.updatedAt).unix();
      const netWorth = balancesToNetWorth({ balances, rates });

      return { x: timeStamp, y: netWorth };
    });

  return (
    <div>
      <Graph
        data={netWorthData}
        xAxisTile="Time"
        yAxisTile="NetWorth"
        xType="time"
      />
    </div>
  );
};

export default App;
