const TRANSACTION_TYPES = {
  CONVERSION: 'conversion',
  EXTERNAL_ACCOUNT: 'external account',
  PEER: 'peer',
};

const DIRECTIONS = {
  DEBIT: 'debit',
  CREDIT: 'credit',
};



const getNewBalances = ({ balances, transaction }) => {
  let getFunction = getNewBalancesSimple;

  if (transaction.type === TRANSACTION_TYPES.CONVERSION) {
    getFunction = getNewBalancesConversion;
  }

  const newBalances = getFunction({ balances, transaction });

  return newBalances;
};


const getNewBalancesSimple = ({ balances, transaction }) => {
  const {
    amount,
  createdAt,
    currency,
    direction,
  } = transaction;

  const newBalances = {
    ...balances,
    currencies: { ...balances.currencies },
  };

  const balance = balances.currencies[currency];
  const newBalance = direction === DIRECTIONS.CREDIT ? balance + amount : balance - amount;

  newBalances.currencies[currency] = newBalance;

  newBalances.updatedAt = createdAt;

  return newBalances;
};


const getNewBalancesConversion = ({ balances, transaction }) => {
  const { createdAt, from, to } = transaction;

  const newBalances = {
    ...balances,
    currencies: { ...balances.currencies },
  };

  newBalances.currencies[from.currency] -= from.amount;
  newBalances.currencies[to.currency] += to.amount;

  newBalances.updatedAt = createdAt;

  return newBalances;
};


export const transactionsToBalancesHistory = ({ transactions }) => {
  const initialBalances = {
    updatedAt: null,
    currencies: {
      BTC: 0,
      CAD: 0,
      ETH: 0,
    },
  };

  let balances = initialBalances;

  let balanceHistory = [];

  transactions.forEach((transaction) => {
    const newBalances = getNewBalances({ balances, transaction });
    balanceHistory.push(newBalances);
    balances = newBalances
  });

  return balanceHistory;
};

export const balancesToNetWorth = ({ balances, rates }) => {
  const { BTC_CAD_rate, ETH_CAD_rate } = rates;

  const cad = balances.currencies.CAD;
  const btcInCad = balances.currencies.BTC * BTC_CAD_rate;
  const ethInCad = balances.currencies.ETH * ETH_CAD_rate;

  const netWorth = cad + btcInCad + ethInCad;

  return netWorth;
};
